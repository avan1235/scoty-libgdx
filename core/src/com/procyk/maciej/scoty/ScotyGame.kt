package com.procyk.maciej.scoty

import com.badlogic.gdx.Game
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.procyk.maciej.scoty.screen.PlayScreen

class ScotyGame : Game() {

    val batch by lazy { SpriteBatch() }

    override fun create() {
        this.setScreen(PlayScreen(this))
    }

    override fun render() {
        super.render()
    }

    override fun dispose() {
        batch.dispose()
    }
}