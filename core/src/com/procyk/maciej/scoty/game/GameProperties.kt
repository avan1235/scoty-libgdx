package com.procyk.maciej.scoty.game

const val V_HEIGHT = 450f
const val V_WIDTH = 800f
const val PPM = 100f
const val UNIT_SCALE = 1f / PPM

const val MOVE_SPEED = 0.1f
const val JUMP_SPEED = 4.5f
const val MAX_SPEED = 2f

const val VELOCITY_INTERACTIONS = 6
const val POSITION_ITERATIONS = 2

const val FRAME_SIZE = 32

inline fun <reified T : Number> withPPM(value: T): Float = value.toFloat() / PPM