package com.procyk.maciej.scoty.sprite

import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TiledMapTile
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.physics.box2d.*
import com.procyk.maciej.scoty.game.withPPM

abstract class TiledObject(protected val map: TiledMap,
                           protected val world: World,
                           protected val bounds: Rectangle) {
    protected val tile: TiledMapTile? = null
    protected val body: Body

    init {
        val bodyDef = BodyDef()
        val shape = PolygonShape()
        val fixDef = FixtureDef()

        bodyDef.type = BodyDef.BodyType.StaticBody
        bodyDef.position.set(withPPM(bounds.x + bounds.width / 2f), withPPM(bounds.y + bounds.height / 2f))

        body = world.createBody(bodyDef)
        shape.setAsBox(withPPM(bounds.width / 2f), withPPM(bounds.height / 2f))
        fixDef.shape = shape
        body.createFixture(fixDef)
    }
}