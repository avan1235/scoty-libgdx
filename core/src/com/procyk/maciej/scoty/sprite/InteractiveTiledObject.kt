package com.procyk.maciej.scoty.sprite

import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.physics.box2d.World

abstract class InteractiveTiledObject(map: TiledMap,
                             world: World,
                             bounds: Rectangle) : TiledObject(map, world, bounds) {
}