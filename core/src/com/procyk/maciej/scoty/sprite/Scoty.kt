package com.procyk.maciej.scoty.sprite

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*
import com.procyk.maciej.scoty.game.*

class Scoty(private val world: World, private val fullTexture: Texture) : Sprite(TextureRegion(fullTexture, 0, 0, 32, 32)) {

    enum class State {
        STANDING,
        RUNNING,
        JUMPING,
        FALLING
    }

    val body: Body
    private val scotyStand = TextureRegion(texture, 0, 0, 32, 32)

    private val runAnimation: Animation<TextureRegion>
    private var state = State.STANDING
    private var previousState = State.STANDING
    private var onRight = true
    private var stateTimer = 0f


    init {
        val frames = Array(9) { TextureRegion(fullTexture, FRAME_SIZE * (it + 1), 0, FRAME_SIZE, FRAME_SIZE) }
        runAnimation = Animation(0.1f, *frames)

        setBounds(0f, 0f, withPPM(FRAME_SIZE), withPPM(FRAME_SIZE))
        setRegion(scotyStand)

        with(BodyDef()) {
            position.set(withPPM(128f), withPPM(192f))
            type = BodyDef.BodyType.DynamicBody
            body = world.createBody(this)
        }
        val fixDef = FixtureDef()
        val circleShape = CircleShape()
        circleShape.radius = withPPM(10f)
        fixDef.shape = circleShape
        body.createFixture(fixDef)
    }

    fun update(delta: Float) {
        setPosition(body.position.x - width / 2, body.position.y - height / 2)
        setRegion(getFrame(delta))
    }

    fun getFrame(delta: Float): TextureRegion {
        state = getCurrentState()

        val region: TextureRegion = when (state) {
            State.RUNNING -> runAnimation.getKeyFrame(stateTimer, true)
            State.FALLING, State.STANDING, State.JUMPING -> TextureRegion(fullTexture, 0, 0, FRAME_SIZE, FRAME_SIZE)
        }

        if ((body.linearVelocity.x < 0 || !onRight) && !region.isFlipX) {
            region.flip(true, false)
            onRight = false
        }
        else if ((body.linearVelocity.x > 0 || onRight) && region.isFlipX) {
            region.flip(true, false)
            onRight = true
        }

        stateTimer = if (previousState == state) stateTimer + delta else 0f
        previousState = state

        return region
    }

    fun getCurrentState(): State = when {
            body.linearVelocity.x != 0f -> {
                State.RUNNING
            }
            body.linearVelocity.y > 0 -> {
                State.JUMPING
            }
            body.linearVelocity.y < 0 -> {
                State.FALLING
            }
            else -> {
                State.STANDING
            }
        }

    fun jump() {
        if (body.linearVelocity.y == 0f) {
            applyVector(y = JUMP_SPEED)
        }
    }

    fun moveRight() {
        if (body.linearVelocity.x < MAX_SPEED) {
            applyVector(x = MOVE_SPEED)
        }
    }

    fun moveLeft() {
        if (body.linearVelocity.x > -MAX_SPEED) {
            applyVector(x = -MOVE_SPEED)
        }
    }

    private fun applyVector(x: Float = 0f, y: Float = 0f) {
        body.applyLinearImpulse(Vector2(x, y), body.worldCenter, true)
    }
}