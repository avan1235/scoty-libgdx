package com.procyk.maciej.scoty.utils

import com.badlogic.gdx.maps.objects.RectangleMapObject
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.FixtureDef
import com.badlogic.gdx.physics.box2d.PolygonShape
import com.badlogic.gdx.physics.box2d.World
import com.procyk.maciej.scoty.game.withPPM
import com.procyk.maciej.scoty.sprite.TiledObject

object WorldCreator {
    fun createBounds(world: World, map: TiledMap, layerName: String) {
        val bodyDef = BodyDef()
        val shape = PolygonShape()
        val fixDef = FixtureDef()

        for (mapObject in map.layers.get(layerName).objects.getByType(RectangleMapObject::class.java)) {
            val rectangle = mapObject.rectangle
            bodyDef.type = BodyDef.BodyType.StaticBody
            bodyDef.position.set(withPPM(rectangle.x + rectangle.width / 2f), withPPM(rectangle.y + rectangle.height / 2f))

            val body = world.createBody(bodyDef)
            shape.setAsBox(withPPM(rectangle.width / 2f), withPPM(rectangle.height / 2f))
            fixDef.shape = shape
            body.createFixture(fixDef)
        }
    }

    fun createRectangleObjects(world: World, map: TiledMap, layerName: String, supplier: (map: TiledMap, world: World, bounds: Rectangle) -> TiledObject) {
        for (mapObject in map.layers.get(layerName).objects.getByType(RectangleMapObject::class.java)) {
            val rectangle = mapObject.rectangle
            supplier(map, world, rectangle)
        }
    }
}