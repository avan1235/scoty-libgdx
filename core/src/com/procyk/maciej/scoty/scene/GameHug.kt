package com.procyk.maciej.scoty.scene

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Disposable
import com.badlogic.gdx.utils.viewport.FitViewport
import com.procyk.maciej.scoty.game.V_HEIGHT
import com.procyk.maciej.scoty.game.V_WIDTH

class GameHug(sprite: SpriteBatch) : Disposable {

    private var timeCounter = 0
    private var scoreCounter = 0
    private var levelCounter = 0

    private val timeLabel = formatIntLabel(timeCounter)
    private val scoreLabel = formatIntLabel(scoreCounter)
    private val levelLabel = formatIntLabel(levelCounter)

    private val timeNameLabel = formatStringLabel("TIME")
    private val scoreNameLabel = formatStringLabel("SCORE")
    private val levelNameLabel = formatStringLabel("LEVEL")

    private val viewport = FitViewport(V_WIDTH, V_HEIGHT, OrthographicCamera())

    val stage = Stage(viewport, sprite)

    init {
        val table = Table()
        with(table) {
            top()
            setFillParent(true)
            add(timeNameLabel).expandX().padTop(10f)
            add(levelNameLabel).expandX().padTop(10f)
            add(scoreNameLabel).expandX().padTop(10f)
            row()
            add(timeLabel).expandX()
            add(levelLabel).expandX()
            add(scoreLabel).expandX()
        }
        stage.addActor(table)
    }

    override fun dispose() {
        stage.dispose()
    }

    private fun formatIntLabel(labelValue: Int, length: Int = 3)
            = formatStringLabel(String.format("%0${length}d", labelValue))

    private fun formatStringLabel(labelValue: String)
            = Label(labelValue, Label.LabelStyle(BitmapFont(), Color.BLACK))
}