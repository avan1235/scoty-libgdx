package com.procyk.maciej.scoty.screen

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.maps.tiled.TmxMapLoader
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*
import com.badlogic.gdx.utils.Disposable
import com.badlogic.gdx.utils.viewport.FitViewport
import com.procyk.maciej.scoty.ScotyGame
import com.procyk.maciej.scoty.game.*
import com.procyk.maciej.scoty.scene.GameHug
import com.procyk.maciej.scoty.sprite.Poop
import com.procyk.maciej.scoty.sprite.Scoty
import com.procyk.maciej.scoty.utils.WorldCreator

class PlayScreen(private val game: ScotyGame) : Screen {

    private val camera = OrthographicCamera()
    private val viewport = FitViewport(withPPM(V_WIDTH), withPPM(V_HEIGHT), camera)
    private val hud = GameHug(game.batch)

    private val mapLoader = TmxMapLoader()
    private val map = mapLoader.load("level1.tmx")
    private val renderer = OrthogonalTiledMapRenderer(map, UNIT_SCALE)

    private val world = World(Vector2(0f, -10f), true)
    private val box2DDebugRenderer = Box2DDebugRenderer()

    private val scoty: Scoty

    init {
        camera.position.set(viewport.worldWidth / 2f, viewport.worldHeight / 2f, 0f)
        WorldCreator.createBounds(world, map, "ground")
        WorldCreator.createRectangleObjects(world, map, "poops", ::Poop)
        scoty = Scoty(world, Texture("scoty.png"))
    }

    override fun hide() {
    }

    override fun show() {
    }

    override fun render(delta: Float) {
        update(delta)

        Gdx.gl.glClearColor(1f, 1f, 1f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        scoty.update(delta)

        renderer.render()
        box2DDebugRenderer.render(world, camera.combined)

        game.batch.projectionMatrix = camera.combined
        game.batch.begin()
        scoty.draw(game.batch)
        game.batch.end()

        game.batch.projectionMatrix = hud.stage.camera.combined
        hud.stage.draw()
    }

    fun update(delta: Float) {
        handleInput(delta)

        world.step(1f / 60f, VELOCITY_INTERACTIONS, POSITION_ITERATIONS)

        camera.position.x = scoty.body.position.x
        camera.update()
        renderer.setView(camera)
    }

    fun handleInput(delta: Float) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
            scoty.jump()
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            scoty.moveRight()
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            scoty.moveLeft()
        }
    }

    override fun pause() = Unit

    override fun resume() = Unit

    override fun resize(width: Int, height: Int) {
        viewport.update(width, height)
    }

    override fun dispose() {
        val disposable = arrayOf(map, hud, renderer, world, box2DDebugRenderer)
        disposable.forEach(Disposable::dispose)
    }
}